import pygame, EntityClass, AnimationClass, Global, FireClass, random

class Projectile(EntityClass.Entity):
    def __init__(self, game):
        EntityClass.Entity.__init__(self,game, False, 110)
        self.game = game
        self.image = pygame.image.load("images/lantern.png").convert_alpha()
        self.image = pygame.transform.scale(self.image, (int(self.image.get_width()*1.5), int(self.image.get_height()*1.5)))
        self.rect = self.image.get_rect()
        self.rect.width = 45
        self.rect.centerx = self.game.player.rect.centerx
        self.rect.centery = self.game.player.rect.top
        if self.game.player.direction == 1:
            if self.game.player.moving[1]:
                self.vel_x = self.game.player.vel_x + 15
            else:
                self.vel_x = 15
        else:
            if self.game.player.moving[0]:
                self.vel_x = -self.game.player.vel_x - 15
            else:
                self.vel_x = -15
        self.vel_y = -12
        self.gravity = 1

        self.anim = AnimationClass.Animation(self.image, 45, 45, 0,3,5)

    def Update(self):
        self.rect.left += self.vel_x
        self.rect.top += self.vel_y
        self.vel_y += self.gravity
        #print self.rect.center
        #print self.game.player.rect.center
        if self.rect.bottom > Global.groundHeight:
            FireClass.Fire(self.game,random.randint(1,5), self.rect.centerx, self.rect.bottom, 1, 0, 0)
            EntityClass.Entity.Destroy(self)

        for entity in self.game.solids:
            if self.rect.colliderect(entity.rect):
                FireClass.Fire(self.game,random.randint(1,5), self.rect.centerx, self.rect.bottom, 1, 0, 0)
                EntityClass.Entity.Destroy(self)
        for entity in self.game.enemies:
            if self.rect.colliderect(entity.rect):
                self.game.player.DamageOpponent(entity, 10)
                EntityClass.Entity.Destroy(self)

    def Draw(self,screen):
        screen.blit(self.anim.Animate(), self.game.camera.apply(self))