#-------------------------------------------------------------------------------
# Name:        Global.py
# Purpose:      a place to store Global Static stuff like screen length ratio
#
# Author:      johnss18
#
# Created:     12/09/2014
# Copyright:   (c) johnss18 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------

#screen length ratio
SL = 1280.0 / 1280.0

groundHeight = 650 * SL