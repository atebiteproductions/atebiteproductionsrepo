import pygame, EntityClass, HillClass, random, AnimationClass

class Fire(EntityClass.Entity):
    def __init__(self, game, size, x, y, speed, castleWidth, castleHeight):
        EntityClass.Entity.__init__(self, game, False, speed)
        #assigns the fire a spritesheet based on the size input, higher numbers meaning larger fire
        self.game = game
        self.size = size
        self.speed = speed
        self.image = pygame.image.load("images/fire_giant.png")
        if size == 1:
            self.image = pygame.image.load("images/fire_tiny.png")
        elif size == 2:
            self.image = pygame.image.load("images/fire_small.png")
        elif size == 3:
            self.image = pygame.image.load("images/fire_medium.png")
        elif size == 4:
            self.image = pygame.image.load("images/fire_large.png")
        elif size == 5:
            self.image = pygame.image.load("images/fire_giant.png")
        self.frame = 0
        self.rect = self.image.get_rect()
        self.x = x
        self.y = y
        self.rect.left = x
        self.rect.top = y-100
        if(castleWidth != 0):
            self.xOffset = random.randint(-castleWidth/2, castleWidth/2)
        else:
            self.xOffset = 0
        if(castleHeight != 0):
            self.yOffset = random.randint(0, castleHeight-(self.image.get_height()/2))
        else:
            self.yOffset = 0
        self.image.set_alpha(random.randint(100,180))

        self.anim = AnimationClass.Animation(self.image, 100, 100, 0,3,random.randint(5,7))
        self.castle = False
        if castleWidth == 0 and castleHeight == 0:
            self.castle = False
        else:
            self.castle = True

    #updates the fires rect so that it can scroll at a different speed than the camera
    def Update(self):
        #AnimationClass.Animation.Animate()
        if self.castle:
            if self.game.currentScreen < 8:
                self.rect = pygame.Rect(self.x + ((self.game.camera.state.right-4000)*self.speed)-480, 250 ,100, 100)
            else:
                self.rect = pygame.Rect(self.x + ((self.game.camera.state.right-4000)*self.speed), 250 ,100, 100)
            #print self.rect.center

    #calls the animation class to get the next frame of the animation, then draws it based on the rect
    def Draw(self, screen):
        if self.castle:
            screen.blit(self.anim.Animate(), pygame.Rect(self.rect.centerx + self.xOffset, self.rect.centery + self.yOffset, 100,100))
        else:
            screen.blit(self.anim.Animate(), self.game.camera.apply(self))