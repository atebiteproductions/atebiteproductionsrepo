#-------------------------------------------------------------------------------
# Name:        Entity
# Purpose:
#
# Author:      johnss18
#
# Created:     07/09/2014
# Copyright:   (c) johnss18 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import pygame, sys, random, time, pygame.display, pygame.time, EntityClass, Global
from AnimationClass import*

class Character(EntityClass.Entity):
    def __init__(self, game, spritePath):
        EntityClass.Entity.__init__(self, game, False , 105)
        self.life = 6
        self.revertImage = None
        self.rect = None

        self.image = pygame.image.load(spritePath).convert_alpha()   #stores the characters sprite sheet
        self.untilRevert = 0
        self.untilCounter = 0

        self.direction = 0 #1 == right, 0 == left
        self.moving = [False, False]
        self.currentXVel= 0

        #self.vel_x = 10* Global.SL
        self.vel_y = 10* Global.SL

        self.isJumping = False

        self.currentBehavior = self.Idling     #will be called as a function and update the character based on a selected behavior function
        #animation variables

        self.walkAnim = Animation(self.image,50,100,0,3,2)
        self.idleAnim = Animation(self.image,50,100,0,1,5)
        self.jumpAnim = Animation(self.image,50,100,7,1,5)
        self.attackAnim = Animation(self.image,50,100,4,2,4)
        self.swordAnim = Animation(self.image,50,100,6,1,5)
        self.recoilAnim = Animation(self.image,50,100,7,2,6)
        self.throwAnim = Animation(self.image,50,100,10,1,6)

        self.spriteToDraw = self.idleAnim.Animate()      #stores the correct portion of the sprite sheet that should be used to be draw

        self.throwCoolDown = 15
        self.throwCount = 0

    def Draw(self, screen):
        '''draws the character'''
        if self.direction == 0:
            self.spriteToDraw = pygame.transform.flip(self.spriteToDraw, True, False)
        #draw my character
        screen.blit(self.spriteToDraw, self.game.camera.apply(self))

        #draw the sword if you are attacking
        if self.currentBehavior == self.Attacking:
            if self.attackAnim.currentFrame == 1:
                if self.direction == 0:                      #if you're facing left, the sword should be left and facing that way too
                    drawSword = pygame.transform.flip(self.swordAnim.Animate(), True, False)
                    drawRect = self.game.camera.apply(self)
                    drawRect.x -= 50
                    screen.blit(drawSword, drawRect)
                if self.direction == 1:                      #same thing for right
                    drawRect = self.game.camera.apply(self)
                    drawRect.x += 50
                    screen.blit(self.swordAnim.Animate(), drawRect)

    def Update(self):
        self.currentBehavior()
        self.UpdateStateMachine()

        self.throwCount += 1


    def UpdateStateMachine(self):
        '''CHARACTER STATE MACHINE --- determine character state and animation'''
        if self.currentBehavior == self.Attacking:
            return
        if self.currentBehavior == self.Throwing:
            return
        if self.isJumping:
            self.currentBehavior = self.Jumping
            return
        if not self.moving[0] and not self.moving[1]:
            self.currentBehavior = self.Idling
            return
        #if you're not moving then you must be idling
        if (self.moving[0] or self.moving[1]) and  not self.isJumping:
            self.currentBehavior = self.Walking
            return

    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    """Behavior Functions, only one will be called every frame"""

    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    def Walking(self):
        self.MovementMotor()
        self.UpdateDirection()
        self.ApplyGravity()
        self.spriteToDraw = self.walkAnim.Animate()
    def Idling(self):
        self.MovementMotor()
        self.spriteToDraw = self.idleAnim.Animate()
    def Jumping(self):
        self.MovementMotor()
        self.ApplyGravity()
        self.spriteToDraw = self.jumpAnim.Animate()
    def Attacking(self):

        self.ApplyGravity()
        #decrease speed
        #self.vel_x -= .25
        attackRect = None

        if self.direction == 0:
            attackRect = self.rect.move(-self.rect.width, 0)
        else:
            attackRect = self.rect.move(self.rect.width, 0)

        for enemy in self.game.enemies:
            if enemy.rect.colliderect(attackRect):
                self.DamageOpponent(enemy, 5)

            #enemy
        if type(self).__name__ == "Knight":
            if self.rect.colliderect(self.game.player.rect):
                self.DamageOpponent(self.game.player, 1)


        self.spriteToDraw = self.attackAnim.Animate()
        if self.attackAnim.total % 8 == 0 and not self.attackAnim.total == 0:
            self.currentBehavior = self.Idling         #end behavior and switch to walking

    def Throwing(self):
        self.MovementMotor()
        self.ApplyGravity()
        self.spriteToDraw = self.throwAnim.Animate()

        if self.throwAnim.done == True:
            self.currentBehavior = self.Idling
    def Recoiling(self):
        self.spriteToDraw = self.recoilAnim.Animate()

    """====================================================================================================="""


    """====================================================================================================="""
    def Jump(self):
        if not self.isJumping:
            self.isJumping = True
            self.vel_y = -20
    def ApplyGravity(self):
        if self.isJumping:
            self.rect.y += self.vel_y
            if self.vel_y > 0:
                for solidObject in self.game.solids:
                    #Check to see if character collides with a solid object
                    if solidObject.rect.colliderect(self.rect):
                        self.rect.bottom = solidObject.rect.top+1
                        self.isJumping = False
                        self.vel_y = 0
                        break
                self.vel_y += 2
            else:
                self.vel_y += 1

            if self.rect.bottom >= Global.groundHeight:
                    self.rect.bottom = Global.groundHeight
                    self.vel_y = 0
                    self.isJumping = False
        else:
            collision = False
            for solidObject in self.game.solids:
                #collision = False
                if solidObject.rect.colliderect(self.rect):
                    collision = True
                    break

            if not collision and self.rect.bottom != Global.groundHeight:
                self.isJumping = True
                self.vel_y = 1
    '''updates the position of the character'''
    def MovementMotor(self):
        pass
    def AtkMovement(self):
        pass #if self. > 0
    def DamageOpponent(self, opponent, damage):
        if self.currentBehavior == self.Recoiling:          #only do damage if they aren't already recoiling
            return
        else:
            opponent.life -= damage
            self.currentBehavior = self.Recoiling
            if opponent.life <= 0:
                opponent.Destroy()

    """Update your rotation and direction"""
    def UpdateDirection(self):
        if self.direction == 0 and self.moving[1]:        #if you face left but are holding right
            self.direction =1
        if self.direction == 1 and self.moving[0]:        #if you face left but are holding right
            self.direction =0