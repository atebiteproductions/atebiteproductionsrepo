import pygame
import CharacterClass
import Global

class Civilian(CharacterClass.Character):
    '''civilian'''
    def __init__(self, game, x, life):
        spritePath = "images/sprites_villager_painted.png"
        CharacterClass.Character.__init__(self, game, spritePath)
        self.life = life
        self.game = game

        self.rect = self.image.get_rect()
        self.rect.width = 50* Global.SL
        self.rect.height = 100* Global.SL
        self.rect.x = x* Global.SL
        self.rect.bottom = Global.groundHeight
        self.vel_x = 4* Global.SL

        self.isRunning = False;

    def Update(self):
        self.vel_x = 6* Global.SL
        CharacterClass.Character.Update(self)

        """updates position of the civilian"""
        if(self.rect.left < 0):
            self.rect.left = 0
        elif self.rect.right > 6000* Global.SL:
            self.rect.right = 6000* Global.SL

        distanceToPlayer = self.game.player.rect.x - self.rect.x
        if abs(distanceToPlayer) < 600* Global.SL:
            self.isRunning = True

        #If not right next to player
        if self.isRunning:
            inFrontOfPlayer = distanceToPlayer > 0

            if inFrontOfPlayer:
                self.rect.x -= self.vel_x
                self.moving[1] = False
                self.moving[0] = True
            else:
                self.rect.x += self.vel_x
                self.moving[0] = False
                self.moving[1] = True

'''
    def Destroy(self):
        self.game.updateList.remove(self)
        self.game.drawList.remove(self)
        self.game.enemies.remove(self)'''
