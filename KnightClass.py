import pygame
import CharacterClass
import Global
import random

class Knight(CharacterClass.Character):
    '''knight - enemy'''
    def __init__(self, game, x, life):
        spritePath = "images/sprites_soldier_painted.png"
        CharacterClass.Character.__init__(self, game, spritePath)
        self.maxLife = life
        self.life = life
        self.game = game

        self.rect = self.image.get_rect()
        self.rect.width = 50* Global.SL
        self.rect.height = 100* Global.SL
        self.rect.x = x* Global.SL
        self.rect.bottom = Global.groundHeight
        self.vel_x = 6* Global.SL

        self.isRunning = False;
        
        self.attackProp = 95
        self.fleeProp = 101
        self.fleeProp = self.fleeProp - ((self.game.currentScreen-1)*2)

    def Update(self):
        #does everything
        CharacterClass.Character.Update(self)

        """updates position of the knight"""

        distanceToPlayer = self.game.player.rect.x - self.rect.x

        #Activate AI when player draws near
        if abs(distanceToPlayer) < 600* Global.SL:
            if self.isRunning:
                inFrontOfPlayer = distanceToPlayer > 0

                if inFrontOfPlayer:
                    self.rect.x -= self.vel_x
                    self.moving[1] = False
                    self.moving[0] = True
                else:
                    self.rect.x += self.vel_x
                    self.moving[0] = False
                    self.moving[1] = True
            else:
                if(self.rect.left < 0):
                    self.rect.left = 0
                elif self.rect.right > 6000* Global.SL:
                    self.rect.right = 6000* Global.SL

                #If not right next to player
                if abs(distanceToPlayer) > self.rect.width * 2:
                    inFrontOfPlayer = distanceToPlayer > 0

                    if inFrontOfPlayer:
                        self.rect.x += self.vel_x
                        self.moving[1] = True
                        self.moving[0] = False
                    else:
                        self.rect.x -= self.vel_x
                        self.moving[0] = True
                        self.moving[1] = False
                else:
                    if random.randrange(100) >= self.attackProp:
                        print 'Knight attack!'
                        self.currentBehavior = self.Attacking
                    elif self.maxLife != self.life and random.randrange(100) >= self.fleeProp:
                        self.isRunning = True
    """
    def Destroy(self):
        self.game.updateList.remove(self)
        self.game.drawList.remove(self)
        self.game.enemies.remove(self)
        """
