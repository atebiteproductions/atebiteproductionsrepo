import pygame, math, EntityClass, random, HillClass, WallClass, CloudClass, FireClass, KnightClass, CivilianClass, WhiteKnightClass

class Map(EntityClass.Entity):
    def __init__(self, game, scene):
        '''initializes the map'''
        EntityClass.Entity.__init__(self, game, False, 110)
        self.game = game
        self.data = open("scene%d.txt" %scene).readlines()
        self.data = [line.rstrip() for line in self.data]
        self.wall1 = pygame.image.load('images/wall_middle_01.png').convert_alpha()
        self.wall2 = pygame.image.load('images/wall_middle_02.png').convert_alpha()
        self.wall3 = pygame.image.load('images/wall_middle_03.png').convert_alpha()
        self.wallLeft1 = pygame.image.load('images/wall_left_01.png').convert_alpha()
        self.wallLeft2 = pygame.image.load('images/wall_left_02.png').convert_alpha()
        self.wallLeft3 = pygame.image.load('images/wall_left_03.png').convert_alpha()
        self.wallRight1 = pygame.image.load('images/wall_right_01.png').convert_alpha()
        self.wallRight2 = pygame.image.load('images/wall_right_02.png').convert_alpha()
        self.wallRight3 = pygame.image.load('images/wall_right_03.png').convert_alpha()
        self.castle1 = pygame.image.load('images/castle.png').convert_alpha()
        self.castle2 = pygame.image.load('images/castle.png').convert_alpha()
        self.hill1 = pygame.image.load('images/background_hill_01.png').convert_alpha()
        self.hill2 = pygame.image.load('images/background_hill_02.png').convert_alpha()
        self.hill3 = pygame.image.load('images/background_hill_03.png').convert_alpha()
        self.mount1 = pygame.image.load('images/background_mountain_01.png').convert_alpha()
        self.mount2 = pygame.image.load('images/background_mountain_02.png').convert_alpha()
        self.mount3 = pygame.image.load('images/background_mountain_03.png').convert_alpha()
        self.mount4 = pygame.image.load('images/background_mountain_04.png').convert_alpha()
        self.dirt1 = pygame.image.load('images/background_dirt_01.png').convert_alpha()
        self.dirt2 = pygame.image.load('images/background_dirt_02.png').convert_alpha()
        self.dirt3 = pygame.image.load('images/background_dirt_03.png').convert_alpha()
        self.dirt4 = pygame.image.load('images/background_dirt_04.png').convert_alpha()
        self.dirt5 = pygame.image.load('images/background_dirt_05.png').convert_alpha()
        self.tree1 = pygame.image.load('images/tree_01.png').convert_alpha()
        self.tree2 = pygame.image.load('images/tree_02.png').convert_alpha()
        self.tree3 = pygame.image.load('images/tree_03.png').convert_alpha()
        self.tree4 = pygame.image.load('images/tree_04.png').convert_alpha()
        self.cottage = pygame.image.load('images/cottage_01_normal.png').convert_alpha()
        self.cottageBurned = pygame.image.load('images/cottage_01_burned.png').convert_alpha()

        #reads through the file one character at a time, creating an instance of the relevant object when it finds the character associated with it
        #objects with multiple sprites randomly determine one
        for i, row in enumerate(self.data):
            for j, column in enumerate(row):
                if column == '1':
                    HillClass.Hill(self.game, self.hill1, j*100-100, "Hill",0)
                elif column == '2':
                    HillClass.Hill(self.game, self.hill2, j*100-100, "Hill",0)
                elif column == '3':
                    HillClass.Hill(self.game, self.hill3, j*100-100, "Hill",0)
                elif column == '4':
                    HillClass.Hill(self.game, self.mount1, j*100-100, "Mountain",0)
                elif column == '5':
                    HillClass.Hill(self.game, self.mount2, j*100-100, "Mountain",0)
                elif column == '6':
                    HillClass.Hill(self.game, self.mount3, j*100-100, "Mountain",0)
                elif column == '7':
                    HillClass.Hill(self.game, self.mount4, j*100-100, "Mountain",0)
                elif column == 'x':
                    temp = random.randint(1,3)
                    if temp == 1:
                        WallClass.Wall(self.game, self.wall1, j*40, i*40)
                    elif temp == 2:
                        WallClass.Wall(self.game, self.wall2, j*40, i*40)
                    elif temp == 3:
                        WallClass.Wall(self.game, self.wall3, j*40, i*40)
                elif column == 'L':
                    temp = random.randint(1,3)
                    if temp == 1:
                        WallClass.Wall(self.game, self.wallLeft1, j*40, i*40)
                    elif temp == 2:
                        WallClass.Wall(self.game, self.wallLeft2, j*40, i*40)
                    elif temp == 3:
                        WallClass.Wall(self.game, self.wallLeft3, j*40, i*40)
                elif column == 'R':
                    temp = random.randint(1,3)
                    if temp == 1:
                        WallClass.Wall(self.game, self.wallRight1, j*40, i*40)
                    elif temp == 2:
                        WallClass.Wall(self.game, self.wallRight2, j*40, i*40)
                    elif temp == 3:
                        WallClass.Wall(self.game, self.wallRight3, j*40, i*40)
                elif column == '8':
                    CloudClass.Cloud(self.game, j*40, i*40)
                elif column == 'T':
                    temp = random.randint(1,3)
                    if temp == 1:
                        HillClass.Tree(self.game, self.tree1, j*40)
                    elif temp == 2:
                        HillClass.Tree(self.game, self.tree2, j*40)
                    elif temp == 3:
                        HillClass.Tree(self.game, self.tree3, j*40)
                    elif temp == 4:
                        HillClass.Tree(self.game, self.tree4, j*40)
                elif column == 'c':
                    HillClass.Tree(self.game, self.cottage, j*40)
                elif column == 'b':
                    HillClass.Tree(self.game, self.cottageBurned, j*40)
                elif column == 'X':
                    CastleHill =  HillClass.Hill(self.game, self.mount2, j*100-100, "Castle Mountain",0)
                    if self.game.currentScreen == 1:
                        for i in range(0,20):
                            FireClass.Fire(game,3, CastleHill.x, CastleHill.rect.top, CastleHill.speed, CastleHill.width, CastleHill.height)
                    if self.game.currentScreen == 2:
                        for i in range(0,15):
                            FireClass.Fire(game,4, CastleHill.x, CastleHill.rect.top, CastleHill.speed, CastleHill.width, CastleHill.height)
                    if self.game.currentScreen == 3:
                        for i in range(0,10):
                            FireClass.Fire(game,5, CastleHill.x, CastleHill.rect.top, CastleHill.speed, CastleHill.width, CastleHill.height)
                    if self.game.currentScreen == 7:
                        for i in range(0,50):
                            FireClass.Fire(game, 5, self.game.screen.get_width()/2+800, 00, CastleHill.speed, CastleHill.width, CastleHill.height)
                elif column == 'V':
                    self.game.enemies.append(CivilianClass.Civilian(self.game, j*40, 10))
                elif column == 'K':
                    #print "Made a knight"
                    self.game.enemies.append(KnightClass.Knight(self.game,j*40, 10))
                    #for knight in self.game.enemies:
                        #print knight.rect.center
                elif column == 'W':
                    self.game.enemies.append(WhiteKnightClass.WhiteKnight(self.game, j*40, 30))


    def Draw(self, screen):
        """draws the map"""
        #draws the dirt across the bottom of the screen
        for i, row in enumerate(self.data):
            for j, column in enumerate(row):

                if column == 'A':
                    screen.blit(self.dirt1, pygame.Rect(self.game.camera.state.left + j*40-40,0, 1280,720))
                elif column == 'B':
                    screen.blit(self.dirt2, pygame.Rect(self.game.camera.state.left + j*40-40,0, 1280,720))
                elif column == 'C':
                    screen.blit(self.dirt3, pygame.Rect(self.game.camera.state.left + j*40-40,0, 1280,720))
                elif column == 'D':
                    screen.blit(self.dirt4, pygame.Rect(self.game.camera.state.left + j*40-40,0, 1280,720))
                elif column == 'E':
                    screen.blit(self.dirt5, pygame.Rect(self.game.camera.state.left + j*40-40,0, 1280,720))