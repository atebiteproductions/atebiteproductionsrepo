import pygame
import CharacterClass
import Global

class Player(CharacterClass.Character):
    '''the player's character'''
    def __init__(self, game):
        spritePath = "images/sprites_minion_painted.png"
        CharacterClass.Character.__init__(self, game, spritePath)
        self.game = game

        self.rect = self.image.get_rect()
        self.rect.width = 50
        self.rect.x = 5900 * Global.SL
        self.rect.bottom = Global.groundHeight
        self.vel_x = 10 * Global.SL
        self.vel_y = 0 * Global.SL
        self.currentGroundHeight = Global.groundHeight
        self.moving = [False, False] #left, right

    '''updates the position of the character'''
    def MovementMotor(self):
        CharacterClass.Character.MovementMotor(self)
        """updates position of the player"""
        if self.moving[0]:
            self.rect.x -=  self.vel_x
            if(self.rect.left < 0):
                self.rect.left = 0
                self.game.ChangeScene(self.game.currentScreen+1)
        elif self.moving[1]:
            self.rect.x += self.vel_x
            if self.rect.right > 6000 * Global.SL:
                self.rect.right = 6000* Global.SL
            if self.game.currentScreen >= 7 and self.rect.right > 1280 * Global.SL:
                self.rect.right = 1280 * Global.SL

    def Destroy(self):
        self.game.gameIsRunning = False
        self.game.gameOver = True
