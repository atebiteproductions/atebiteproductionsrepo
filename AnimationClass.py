#-------------------------------------------------------------------------------
# Name:        animation
# Purpose:
#
# Author:      johnss18
#
# Created:     16/09/2014
# Copyright:   (c) johnss18 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import pygame, sys, random, pygame.display, pygame.time, EntityClass, Global

class Animation(object):
    def __init__(self, spriteSheet,spritelength, spriteHeight, startPanel, frames,speed):
        self.image = spriteSheet
        self.startPanel = startPanel
        self.frames = frames
        self.speed = speed
        self.spriteLength = spritelength
        self.spriteHeight = spriteHeight

        self.currentFrame = 0
        self.count = 0
        self.done = False

        self.total = 0

    #this function will return the image that is the next frame of the animation
    def Animate(self):
        self.count += 1
        self.done = False
        self.total += 1

        print self.currentFrame
        if self.count == self.speed:        #if the counter has reached the length to display this frame
            self.count = 0      #reset timer
            self.currentFrame += 1
            if self.currentFrame == self.frames:       #go back to start of animation if its a loop
                self.currentFrame = 0
                self.done = True

        drawRect = pygame.Rect(self.startPanel*self.spriteLength* Global.SL + self.currentFrame*self.spriteLength*Global.SL, 0, self.spriteLength,self.spriteHeight)
        return self.image.subsurface(drawRect)