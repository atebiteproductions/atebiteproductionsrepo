import pygame, EntityClass, random

class Wall(EntityClass.Entity):
    def __init__(self, game, image, x, y):
        EntityClass.Entity.__init__(self,game, True, 100)
        self.game = game
        self.image = image
        self.x = x
        self.y = y+10
        self.rect = image.get_rect()
        self.rect.left = x
        self.rect.top = y+10

    def Update(self):
        pass
        #self.rect = pygame.Rect(self.game.camera.state.left + self.x,self.game.camera.state.top + self.y, 40,100)

    #draws the Wall object relative to the camera
    def Draw(self, screen):
        screen.blit(self.image, self.game.camera.apply(self))