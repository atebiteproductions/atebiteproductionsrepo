import pygame, EntityClass, HillClass, random

class Cloud(HillClass.Hill):
    def __init__(self,game,x,y):
        #randomly determines which cloud sprite to use, how fast it should move, scales it to a random size between .4 and .6 then rotates it to a random rotation
        rand = random.randint(1,7)
        self.image = pygame.image.load('images/background_cloud_01.png').convert_alpha()
        if rand == 1:
            self.image = pygame.image.load('images/background_cloud_01.png').convert_alpha()
        elif rand == 2:
            self.image = pygame.image.load('images/background_cloud_02.png').convert_alpha()
        elif rand == 3:
            self.image = pygame.image.load('images/background_cloud_03.png').convert_alpha()
        elif rand == 4:
            self.image = pygame.image.load('images/background_cloud_04.png').convert_alpha()
        elif rand == 5:
            self.image = pygame.image.load('images/background_cloud_05.png').convert_alpha()
        elif rand == 6:
            self.image = pygame.image.load('images/background_cloud_06.png').convert_alpha()
        elif rand == 7:
            self.image = pygame.image.load('images/background_cloud_07.png').convert_alpha()
        self.speed = random.uniform(.05,.20)
        self.x = x
        self.y = y-80 + random.randint(-20,20)
        HillClass.Hill.__init__(self,game,self.image, self.x,  "Cloud", self.speed/10)
        self.image.set_alpha(150)
        self.size = random.uniform(.4,.6)
        self.image = pygame.transform.scale(self.image, (int(self.image.get_width()*self.size), int(self.image.get_height()*self.size)))
        self.image = pygame.transform.rotate(self.image, random.randint(0,360))

    #clouds move to the right based on their speed, looping when they reach the left
    def Update(self):
        self.x += self.speed*50
        if self.x > 6000:
            self.x = 0
    #draws the cloud, using its speed to have it scroll slower than the camera
    def Draw(self, screen):
        screen.blit(self.image, pygame.Rect(self.x + ((self.game.camera.state.right-4000)*self.speed)-1000, self.y-40, 1280,720))
        #screen.blit(self.image, pygame.Rect(self.x, self.y-40, 1280,720))