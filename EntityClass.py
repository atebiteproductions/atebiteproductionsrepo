#-------------------------------------------------------------------------------
# Name:        Entity
# Purpose:
#
# Author:      johnss18
#
# Created:     07/09/2014
# Copyright:   (c) johnss18 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import pygame, sys, random, pygame.display, pygame.time
class Entity(object):

    def __init__(self, game, is_solid = False ,drawDepth = 100):
        self.game = game
        self.depth = drawDepth
        #print self.depth
        self.isSolid = is_solid
        if is_solid:
            self.game.solids.append(self)

        #print self.game
        self.game.updateList.append(self)
        self.game.drawList.append(self)

        self.game.drawList = sorted(self.game.drawList, key = lambda entity: entity.depth )

    def Update(self):
        pass

    def Draw(self, screen):
        pass

    #removes the entity from drawList and updateList then deletes it
    def Destroy(self):
        if self in self.game.drawList:
            self.game.drawList.remove(self)
        if self in self.game.updateList:
            self.game.updateList.remove(self)
        del self