import pygame, sys, os, re, math, PlayerClass, KnightClass, CivilianClass, CameraClass, MapClass, Global, EntityClass, CloudClass, FireClass, ProjectileClass

menuItems =[
["Start New Game", "RunGame"],
["Stage Selector", "ShowStageSelectorMenu"],
["Exit", "ExitGame"]
]

def DrawVictoryScreen(game):
    game.screen.fill((0, 0, 0))
    screenCenterHorizontal = game.screen.get_rect().width / 2
    screenCenterVertical = game.screen.get_rect().height / 2
    image = pygame.image.load('images/title_victory.png').convert_alpha()
    game.screen.blit(image, image.get_rect(centerx=screenCenterHorizontal, centery=screenCenterVertical))
    pygame.display.flip()

    breakLoop = False
    while not breakLoop:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and (event.key == pygame.K_ESCAPE or event.key == pygame.K_RETURN):
                breakLoop = True

def DrawTextAcrossScreen(text, game, requirePlayerAction = True):
    font = pygame.font.Font("forced_square.ttf", 50)
    game.screen.fill((0, 0, 0))
    screenCenterHorizontal = game.screen.get_rect().width / 2
    screenCenterVertical = game.screen.get_rect().height / 2
    renderedItem = font.render(text, 1, (247, 150, 36), (0, 0, 0))
    game.screen.blit(renderedItem, renderedItem.get_rect(centerx=screenCenterHorizontal, centery=screenCenterVertical))
    pygame.display.flip()

    if requirePlayerAction:
        breakLoop = False
        while not breakLoop:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    breakLoop = True

def DrawImageAcrossScreen(image, game, requirePlayerAction = True):
    game.screen.fill((0, 0, 0))
    screenCenterHorizontal = game.screen.get_rect().width / 2
    screenCenterVertical = game.screen.get_rect().height / 2
    game.screen.blit(image, image.get_rect(centerx=screenCenterHorizontal, centery=screenCenterVertical))
    pygame.display.flip()

    if requirePlayerAction:
        breakLoop = False
        while not breakLoop:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                        breakLoop = True

def DrawTitleAcrossScreen(image1, image2,  game, requirePlayerAction = True):
    #game.screen.fill((0, 0, 0))
    screenCenterHorizontal = game.screen.get_rect().width / 2
    screenCenterVertical1 = game.screen.get_rect().height / 3
    screenCenterVertical2 = game.screen.get_rect().height* 2 / 3 - 100
    game.screen.blit(pygame.image.load("images/background_nightSky.png"), pygame.Rect(0,0,1280,720))
    game.screen.blit(image1, image1.get_rect(centerx=screenCenterHorizontal, centery=screenCenterVertical1))
    game.screen.blit(image2, image2.get_rect(centerx=screenCenterHorizontal, centery=screenCenterVertical2))
    pygame.display.flip()

    if requirePlayerAction:
        breakLoop = False
        alpha = image2.get_alpha()
        while not breakLoop:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                        breakLoop = True

class Game(object):
    updateList = []
    drawList = []

    enemies = []
    solids = []

    gameIsWon = False

    def __init__(self):
        """initializes the game"""
        pygame.init()
        pygame.display.set_caption("Ate Bite Productions")

        self.currentScreen = 1

        #declare member variables
        self.screen = pygame.display.set_mode((1280, 720))

        DrawImageAcrossScreen(pygame.image.load('images/title_loading.png').convert_alpha(), self, False)
        self.clock = pygame.time.Clock()
        self.map = MapClass.Map(self, 1)
        self.player = PlayerClass.Player(self)
        self.camera = CameraClass.Camera(self, CameraClass.complex_camera, 6000* Global.SL, 480* Global.SL)

        self.gameIsRunning = False
        self.gameOver = False


        self.back = pygame.image.load('images/background_nightSky.png').convert()
        self.heart = pygame.image.load('images/ui_heart_light.png').convert_alpha()
        self.darkHeart = pygame.image.load('images/ui_heart_dark.png').convert_alpha()

        self.stabSound = pygame.mixer.Sound("sfx/stab_01.wav")
        self.throwSound = pygame.mixer.Sound("sfx/throw_01.wav")

        #DrawImageAcrossScreen(pygame.image.load('images/title_retribution.png').convert_alpha(), self)
        DrawTitleAcrossScreen(pygame.image.load('images/title_retribution.png').convert_alpha(), pygame.image.load('images/title_insert_coin.png').convert_alpha(), self)
        DrawImageAcrossScreen(pygame.image.load('images/title_exposition.png').convert_alpha(), self)

    def ProcessEvents(self):
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE and not self.player.moving[0] and not self.player.moving[1]:
                    resumeMenuItem = ("Resume Game", "RunGame")
                    if menuItems[0] != resumeMenuItem:
                        menuItems.insert(0, resumeMenuItem)
                    menuItems[1][1] = "RestartGame"
                    pygame.mixer.music.load("music/menu.mid")
                    pygame.mixer.music.play(-1)
                    self.gameIsRunning = False
                if event.key == pygame.K_LEFT:
                    self.player.moving[0] = True
                if event.key == pygame.K_RIGHT:
                    self.player.moving[1] = True
                if event.key == pygame.K_SPACE:
                    self.player.Jump()
                if not self.player.currentBehavior == self.player.Recoiling:
                    if event.key == pygame.K_z:
                        if not (self.player.currentBehavior == self.player.Attacking) and not (self.player.currentBehavior == self.player.Throwing):
                            self.player.currentBehavior = self.player.Attacking
                            self.stabSound.play()
                    if event.key == pygame.K_x:
                        if not (self.player.currentBehavior == self.player.Attacking) and not (self.player.currentBehavior == self.player.Throwing):
                            self.player.currentBehavior = self.player.Throwing
                            self.throwSound.play()
                            latern = ProjectileClass.Projectile(self)
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    self.player.moving[0] = False
                if event.key == pygame.K_RIGHT:
                    self.player.moving[1] = False
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

    def Update(self):
        #print self.updateList
        self.drawList = sorted(self.drawList, key = lambda entity: entity.depth )
        for entity in self.updateList:
            entity.Update()

    def Draw(self):
        self.screen.blit(self.back, pygame.Rect(0,0,1280,720))

        for theEntity in self.drawList:
            theEntity.Draw(self.screen)


        for i in range(0, self.player.life):
            self.screen.blit(self.heart, pygame.Rect(i*40+10,10,30,30))



    #deletes Hills, Walls, Clouds, and Fires, then creates a new Map for the level number indicated by scene
    def ChangeScene(self, scene):
        DrawImageAcrossScreen(pygame.image.load('images/title_loading.png').convert_alpha(), self, False)
        if os.path.isfile("scene%d.txt" %(scene)):
            self.solids = []
            self.currentScreen = scene
            if scene < 7:
                self.player.rect.x = 5900 * Global.SL
                self.camera = CameraClass.Camera(self, CameraClass.complex_camera, 6000* Global.SL, 480* Global.SL)
            else:
                self.player.rect.x = 1180 * Global.SL
                self.camera = CameraClass.Camera(self, CameraClass.complex_camera, 1280* Global.SL, 480* Global.SL)
            i = 0
            while i < len(self.updateList):
            #for i in range(0, len(self.updateList)):
                if type(self.updateList[i]).__name__ == "Knight" or type(self.updateList[i]).__name__ == "Civilian" or type(self.updateList[i]).__name__ == "Wall" or  type(self.updateList[i]).__name__ == "Hill" or  type(self.updateList[i]).__name__ == "Cloud" or  type(self.updateList[i]).__name__ == "Fire" or  type(self.updateList[i]).__name__ == "Tree":
                    EntityClass.Entity.Destroy(self.updateList[i])
                else:
                    i += 1
                    #self.tempList.append(self.updateList[i])
            del self.map
            self.map = MapClass.Map(self, scene)
            #self.player = PlayerClass.Player(self)
            #self.camera = CameraClass.Camera(self, CameraClass.complex_camera, 6000* Global.SL, 480* Global.SL)

    def run(self):
        g.clock.tick(30)
        g.ProcessEvents()
        g.Update()
        g.Draw()
        pygame.display.flip()
        pygame.display.set_caption("Ate Bite Productions  FPS: %.4f" %(self.clock.get_fps()))

g = Game()
pygame.mixer.music.load("music/menu.mid")
pygame.mixer.music.play(-1)

menuFont = pygame.font.Font("forced_square.ttf", 32)
def DrawMenu(menu, breakAtReturn):
    menuStartLength = len(menu)
    selectedItem = 0
    breakLoop = False
    while not breakLoop:
        g.screen.fill((0, 0, 0))
        verticalStart= None
        screenCenterHorizontal = g.screen.get_rect().width / 2

        counter = 0
        for menuItem in menu:
            renderedItem = menuFont.render(" " + menuItem[0] + " ", 1, (247, 150, 36), (0, 0, 0))

            if selectedItem == counter:
                renderedItem = menuFont.render(" " + menuItem[0] + " ", 1, (247, 150, 36), (255, 255, 255))

            if verticalStart == None:
                menuRect = renderedItem.get_rect().move(0,0)
                menuRect.height = renderedItem.get_rect().height * len(menu)
                menuRect.centery = g.screen.get_rect().height / 2
                verticalStart = menuRect.top

            g.screen.blit(renderedItem, renderedItem.get_rect(centerx=screenCenterHorizontal, top=verticalStart+counter*renderedItem.get_rect().height))
            counter += 1

        g.clock.tick(30)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    if selectedItem > 0:
                        selectedItem -= 1
                if event.key == pygame.K_DOWN:
                    if selectedItem < len(menu)-1:
                        selectedItem += 1
                if event.key == pygame.K_RETURN and menu[selectedItem][1] != None:
                    if breakAtReturn:
                        breakLoop = True
                    globals()[menu[selectedItem][1]](selectedItem)
                    if g.gameOver:
                        DrawImageAcrossScreen(pygame.image.load('images/title_game_over.png').convert_alpha(), g)
                        if menuStartLength != len(menu):
                            del menu[0]
                        menu[0][1] = "RestartGame"
                if event.key == pygame.K_ESCAPE and breakAtReturn:
                    breakLoop = True
            if event.type == pygame.KEYUP:
                pass
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

        pygame.display.flip()

def ShowStageSelectorMenu(selectedItem):
    menuItems =[
    ]

    for fn in os.listdir('.'):
        if os.path.isfile(fn) and fn.startswith("scene"):
            menuItems.append(["Stage " + re.sub("[^0-9]", "", str(fn)), "LoadStage"])
    DrawMenu(menuItems, True)

def RunGame(selectedItem):
    pygame.mixer.music.load("music/main_levels.mid")
    pygame.mixer.music.play(-1)
    g.gameIsRunning = True
    while (g.gameIsRunning):
        g.clock.tick(30)
        g.ProcessEvents()
        g.Update()
        g.Draw()
        pygame.display.flip()
        pygame.display.set_caption("Ate Bite Productions  FPS: %.4f" %(g.clock.get_fps()))

        if (g.gameIsWon):
            g.gameIsRunning = False
            g.gameIsWon = False
            DrawVictoryScreen(g)
            g.updateList = []
            g.drawList = []
            g.enemies = []
            g.solids = []
            g.__init__()


def LoadStage(selectedItem):
    g.solids = []
    g.ChangeScene(selectedItem + 1)
    RunGame(selectedItem)

def RestartGame(selectedItem):
    g.updateList = []
    g.drawList = []
    g.enemies = []
    g.solids = []
    g.__init__()
    RunGame(selectedItem)

def ExitGame(selectedItem):
    pygame.quit()
    sys.exit()


DrawMenu(menuItems, False)
