import pygame
import CharacterClass
import Global
import random

class WhiteKnight(CharacterClass.Character):
    '''knight - enemy'''
    def __init__(self, game, x, life):
        spritePath = "images/sprites_knight_painted.png"
        CharacterClass.Character.__init__(self, game, spritePath)
        self.maxLife = life
        self.life = life
        self.game = game

        self.rect = self.image.get_rect()
        self.rect.width = 50* Global.SL
        self.rect.height = 100* Global.SL
        self.rect.x = x* Global.SL
        self.rect.bottom = Global.groundHeight
        self.vel_x = 7* Global.SL

        self.isRunning = False;

        self.attackProp = 85

    def Update(self):
        """inherited from Character, updates the rotation and direction"""
        CharacterClass.Character.Update(self)
        if self.life <= 6:
            print "You win"
            self.game.gameIsWon = True


        """updates position of the knight"""

        distanceToPlayer = self.game.player.rect.x - self.rect.x

        #Activate AI when player draws near
        if abs(distanceToPlayer) < 600* Global.SL:
            if self.isRunning:
                inFrontOfPlayer = distanceToPlayer > 0

                if inFrontOfPlayer:
                    self.rect.x -= self.vel_x
                    self.moving[1] = False
                    self.moving[0] = True
                else:
                    self.rect.x += self.vel_x
                    self.moving[0] = False
                    self.moving[1] = True
            else:
                if(self.rect.left < 0):
                    self.rect.left = 0
                elif self.rect.right > 6000* Global.SL:
                    self.rect.right = 6000* Global.SL

                #If not right next to player
                if abs(distanceToPlayer) > self.rect.width * 2:
                    inFrontOfPlayer = distanceToPlayer > 0

                    if inFrontOfPlayer:
                        self.rect.x += self.vel_x
                        self.moving[1] = True
                        self.moving[0] = False
                    else:
                        self.rect.x -= self.vel_x
                        self.moving[0] = True
                        self.moving[1] = False
                else:
                    if random.randrange(100) >= self.attackProp:
                        print 'Knight life: ' + str(self.life)
                        self.currentBehavior = self.Attacking
                    elif self.maxLife != self.life and random.randrange(10000) >= 9975:
                        self.isRunning = True
    '''def Destroy(self):
        print "You win"'''