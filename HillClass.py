import pygame, EntityClass, random

class Hill(EntityClass.Entity):
    def __init__(self, game, image, x, style, speed):
        #assigns the object a different speed depending upon which type of hill it is, lower numbers are farther back and travel slower
        if(style == "Mountain"):
            self.speed = random.uniform(.05,.15)
        elif style == "Castle Mountain":
            self.speed = .15
        elif(style == "Hill"):
            self.speed = random.uniform(.15,.25)
            #if True: #random.randint(0,5) == 3:
                #Cottage(game, pygame.image.load('images/cottage_burned.png').convert_alpha(), x, self.speed)
        else:
            self.speed = speed

        EntityClass.Entity.__init__(self,game, False,  self.speed)
        self.game = game
        self.image = image
        self.x = x
        self.style = style
        self.castle = pygame.image.load("images/background_castle_dark.png")
        if self.game.currentScreen <= 3:
            self.castle = pygame.image.load("images/background_castle_dark.png")
            self.castle = pygame.transform.scale(self.castle, (int(self.castle.get_width()/self.game.currentScreen), int(self.castle.get_height()/self.game.currentScreen)))
        else:
            self.castle = pygame.image.load("images/background_castle_light.png")
            if self.game.currentScreen < 7:
                self.castle = pygame.transform.scale(self.castle, (int(self.castle.get_width()/(7-self.game.currentScreen)), int(self.castle.get_height()/(7-self.game.currentScreen))))
            elif self.game.currentScreen == 7:
                self.castle = pygame.transform.scale(self.castle, (int(self.castle.get_width()/.5), int(self.castle.get_height()/.5)))
        #else:
           #self.castle = pygame.transform.scale(self.castle, (int(self.castle.get_width()/2), int(self.castle.get_height()/2)))
        self.rect = self.castle.get_rect()
        self.width = self.castle.get_width()
        self.height = self.castle.get_height()


    #updates the position of the hill to scroll relative to the camera movement, speeds lower than 1 will travel slower, faster than 1 will move faster than the camera
    def Update(self):
        self.rect = pygame.Rect(self.x + ((self.game.camera.state.right-4000)*self.speed)-1000, -40, 1280, 720)

    #blits the hill, and if there's a castle, draws that too
    def Draw(self, screen):
        screen.blit(self.image, self.rect)
        if self.style == "Castle Mountain":
            if self.game.currentScreen < 7:
                screen.blit(self.castle, pygame.Rect(self.x+(self.image.get_width()/2)-(self.castle.get_width()/2) + ((self.game.camera.state.right-4000)*self.speed+.01)-1000,self.rect.centery+35, 1280,720))
            elif self.game.currentScreen == 7:
                screen.blit(self.castle, pygame.Rect(1280/2-self.castle.get_width()/2, 720/2-self.castle.get_height()/2,self.castle.get_width(), self.castle.get_height()))
#tree objects behave similarly to hills, but travel faster and are smaller
class Tree(EntityClass.Entity):
    def __init__(self, game, image, x):
        self.speed = random.uniform(.25,.35)
        EntityClass.Entity.__init__(self,game, False, self.speed)
        self.x = x
        self.image = image
        self.rect = image.get_rect()

    def Update(self):
        self.rect = pygame.Rect(self.x + ((self.game.camera.state.right-4000)*self.speed)-1000, 510, self.image.get_width(), self.image.get_height())

    def Draw(self, screen):
        screen.blit(self.image, self.rect)

class Cottage(EntityClass.Entity):
    def __init__(self, game, image, x, speed):
        self.speed = speed + .001
        EntityClass.Entity.__init__(self,game, False, self.speed)
        self.x = x
        self.image = image
        self.yOffset = random.randint(-20,20)
        self.rect = image.get_rect()
        if self.game.currentScreen <= 3:
            self.image = pygame.transform.scale(self.image, (self.image.get_width()/self.game.currentScreen, self.image.get_height()/self.game.currentScreen))

    def Update(self):
        self.rect = pygame.Rect(self.x + ((self.game.camera.state.right-4000)*self.speed)-1000, 500 + self.yOffset, self.image.get_width(), self.image.get_height())

    def Draw(self,screen):
        screen.blit(self.image, self.rect)
