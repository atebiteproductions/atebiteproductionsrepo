import pygame
import EntityClass

WIN_WIDTH = 1280
WIN_HEIGHT = 720
HALF_WIDTH = int(WIN_WIDTH/2)
HALF_HEIGHT = int(WIN_HEIGHT/2)

class Camera(EntityClass.Entity):
    def __init__(self, game, camera_func, width,height):
        EntityClass.Entity.__init__(self, game)
        self.camera_func = camera_func
        self.state = pygame.Rect(0,0,width,height)
        self.target = game.player
        self.state.right = 6000

    #applies the camera to move based on target's location
    def apply(self,target):
        return target.rect.move(self.state.topleft)

    def Update(self):
        #self.state.centerx = -self.game.player.rect.centerx+6000
        #print self.state.center
        #print self.game.player.rect.center
        #if self.game.player.rect.right
        self.state = self.camera_func(self.state, self.target.rect)





#not part of the class
#defines the camera function, moves if the player gets 200 pixels to the right of the center
def complex_camera(camera,target_rect):
    l,t,_,_ = target_rect
    _,_,w,h = camera
    l,t,_,_ = -l+HALF_WIDTH+200, -t + HALF_HEIGHT, w, h

    l = min(0,l)
    l = max(-(camera.width-WIN_WIDTH),l)
    t = max(-(camera.height-WIN_HEIGHT),t)
    t = min(0,t)

    return pygame.Rect(l,t,w,h)